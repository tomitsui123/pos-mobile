package com.tomi.mobile.others

import android.app.Activity
import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.db.entities.Order
import recieptservice.com.recieptservice.PrinterInterface
import java.text.SimpleDateFormat

class PrintService(private var activity: Activity) {

    private lateinit var mAidl: PrinterInterface
    private var serviceConn = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mAidl = PrinterInterface.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
        }
    }

    fun printReceipt(currentOrder: Order) {
        try {
            setMAidlTextSize(70)
            mAidl.setAlignment(1)
            val id = currentOrder.id
            var dateFormatter = SimpleDateFormat("dd/MM/YYYY HH:mm:ss")
            val createdDate = dateFormatter.format(currentOrder.createdDate)
            mAidl.printText("#$id\n\n")
            setMAidlTextSize(25)
            mAidl.setAlignment(0)
            mAidl.printText("日期：${createdDate}")
            mAidl.printText("\n已付：${if(currentOrder.paid) "已付" else "未付"}")
            currentOrder.telephone?.let {
                if (!it.isBlank()) {
                    mAidl.printText("\n電話：${currentOrder.telephone}")
                }
            }
            mAidl.printText("\n\n\n\n\n\n")
//                        title
            setMAidlTextSize(35)
            mAidl.printTableText(arrayOf("數量", "項目", "價錢"), intArrayOf(1, 2, 1), intArrayOf(2, 0, 2))
            mAidl.printText("---------------------")
            val itemList = currentOrder.itemList
            var prevTitle: String = ""
            mAidl.printText("${prevTitle}\n")
            for (item in itemList!!) {
                val currentTitle = item.menuProperty.category
                if (currentTitle != prevTitle) {
                    mAidl.printText("====\n")
                    mAidl.printText("${currentTitle}\n")
                    mAidl.printText("====\n")
                    prevTitle = currentTitle
                }
                printContent(item)
            }
            mAidl.printText("\n")
            mAidl.printTableText(arrayOf("", "總結：", currentOrder.total.toString())
                , intArrayOf(1, 2, 1), intArrayOf(0, 2, 2))
            mAidl.printText("---------------------")
            mAidl.printText("\n\n\n")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun bind() {
        var intent = Intent()
        intent.setClassName("recieptservice.com.recieptservice",
            "recieptservice.com.recieptservice.service.PrinterService")
        activity.bindService(intent, serviceConn, Service.BIND_AUTO_CREATE)
    }

    fun unbind() {
        try {
            activity.unbindService(serviceConn)
        } catch (e: Exception) {
            println(e)
        }
    }

    private fun setMAidlTextSize(textSize: Int) {
        var textSize : Float = textSize.toFloat()
        mAidl.setTextSize(textSize)
    }

    private fun printContent(item: Item) {
        val lumsum = (item.amount * item.menuProperty.price).toString()
        var content = arrayOf(item.amount.toString(), item.menuProperty.displayName, lumsum)
        mAidl.printTableText(content, intArrayOf(1, 2, 1), intArrayOf(2, 0, 2))
        for (remark in item.remarkList!!) {
            mAidl.setAlignment(0)
            mAidl.printText("\n $remark")
        }
        mAidl.printText("\n\n\n\n")
    }
}