package com.tomi.mobile.adapters.baseAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.tomi.mobile.R
import com.tomi.mobile.db.entities.Item
import kotlinx.android.synthetic.main.content_product_details.view.item_name
import kotlinx.android.synthetic.main.product_list.view.*


class SummaryItemAdapter(context: Context, fragment: Fragment?, var itemList: ArrayList<Item>) : BaseAdapter() {
    private var callbackInterface = (fragment ?: context) as CallbackInterface

    interface CallbackInterface {
        fun onHandleSelection(item: Item)
        fun onHandleDelete(item: Item)
    }
    var context: Context? = context

    override fun getItem(position: Int): Any {
        return itemList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return itemList.size
    }


    fun setData(itemList: ArrayList<Item>) {
        this.itemList = itemList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = this.itemList[position]
        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val productListView = inflator.inflate(R.layout.product_list, null)
        productListView.item_name.text = item.menuProperty.displayName
        productListView.num_of_item.text = item.amount.toString()
        productListView.sub_total.text = (item.menuProperty?.price * item.amount).toString()
        productListView.setOnClickListener{
            callbackInterface.onHandleSelection(item)
        }
        val closeBtn = productListView.findViewById(R.id.close_btn) as ImageView
        closeBtn.setOnClickListener {
            callbackInterface.onHandleDelete(item)
        }
        for (remark in item.remarkList!!) {
            val remarkTv = TextView(context)
            remarkTv.text = remark
            productListView.details_container.addView(remarkTv)
        }
        return productListView
    }
}