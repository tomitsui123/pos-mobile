package com.tomi.mobile.adapters.FragmentAdapters

import com.tomi.mobile.ProductListFragment
import com.tomi.mobile.SummaryOrderFragment

class OrderAdapter(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): androidx.fragment.app.Fragment? = when (position) {
        0 -> ProductListFragment.newInstance()
        1 -> SummaryOrderFragment.newInstance()
        else -> null
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "落單"
        1 -> "帳單總結"
        else -> ""
    }

    override fun getCount(): Int = 2
}