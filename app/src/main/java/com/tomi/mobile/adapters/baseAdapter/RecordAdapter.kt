package com.tomi.mobile.adapters.baseAdapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tomi.mobile.ConfirmActivity
import com.tomi.mobile.R
import com.tomi.mobile.db.entities.Order
import kotlinx.android.synthetic.main.record_list.view.*

class RecordAdapter(context: Context, private var orderList: ArrayList<Order>) : BaseAdapter() {
    var context: Context? = context

    override fun getItem(position: Int): Any {
        return orderList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return orderList.size
    }

    fun setData(orderList: ArrayList<Order>) {
        this.orderList = orderList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val order: Order = this.orderList[position]
        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val recordListView = inflator.inflate(R.layout.record_list, null)
        if (order.isSync) {
            recordListView.not_sync.setTextColor(Color.TRANSPARENT)
        }
        if (!order.isEdited) {
            recordListView.edited.setTextColor(Color.TRANSPARENT)
        }
        recordListView.order_number.text = order.id.toString()
        var total = 0
        for (item in order.itemList!!) {
            total += item.menuProperty.price * item.amount
        }

        recordListView.total_amount.text = total.toString()
        recordListView.setOnClickListener{
            var intent = Intent(context, ConfirmActivity::class.java)
            intent.putExtra("id", order.id)
            this.context!!.startActivity(intent)
        }

        return recordListView
    }
}