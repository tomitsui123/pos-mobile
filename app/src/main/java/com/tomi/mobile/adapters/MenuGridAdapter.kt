package com.tomi.mobile.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tomi.mobile.databinding.ProductBoxBinding
import com.tomi.mobile.network.MenuProperty

class MenuGridAdapter(
    listener: OnItemClickListener
) : ListAdapter<MenuProperty, MenuGridAdapter.MenuViewHolder>(DiffCallback) {

    interface OnItemClickListener {
        fun onItemClick(menuProperty: MenuProperty)
    }

    private var mOnItemClickListener = listener
    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1

    override fun getItemViewType(position: Int): Int {
        return when(position) {
            0 -> TYPE_HEADER
            else -> TYPE_ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return when(viewType) {
            TYPE_HEADER -> MenuViewHolder(ProductBoxBinding.inflate(
                LayoutInflater.from(parent.context)))
            else -> MenuViewHolder(ProductBoxBinding.inflate(
                LayoutInflater.from(parent.context)))
        }
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val menuProperty = getItem(position)
        holder.bind(menuProperty, mOnItemClickListener)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<MenuProperty>() {
        override fun areItemsTheSame(oldItem: MenuProperty, newItem: MenuProperty): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: MenuProperty, newItem: MenuProperty): Boolean {
            return oldItem.id === newItem.id
        }
    }

    class MenuViewHolder(private var binding: ProductBoxBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(menuProperty: MenuProperty, listener: OnItemClickListener) {
            binding.property = menuProperty
            binding.executePendingBindings()
            itemView.setOnClickListener {
                listener.onItemClick(menuProperty)
            }
        }
    }
}