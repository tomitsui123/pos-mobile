package com.tomi.mobile.adapters

import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tomi.mobile.R
import com.tomi.mobile.databinding.ProductMainBinding
import com.tomi.mobile.network.MenuProperty


@BindingAdapter("imageUrl")
fun setImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("http").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions().placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image))
            .into(imgView)
    }
}

@BindingAdapter("listData")
fun bindView(recycleListView: RecyclerView, data: List<MenuProperty>?) {
    val adapter = recycleListView.adapter as MenuGridAdapter
    adapter.submitList(data)
}

@BindingAdapter("itemName")
fun setItemName(textView: TextView, displayName: String?) {
    displayName?.let {
        textView.text = it
    }
}