package com.tomi.mobile

interface popUpWindowInterface {
    var isClick: Boolean
    fun onButtonControlPopUp()
    fun initPopUpWindow()
    fun onCloseWindow()
    fun onPrintReceipt()
}