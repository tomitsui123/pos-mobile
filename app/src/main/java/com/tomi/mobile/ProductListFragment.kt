package com.tomi.mobile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.ProductDetailsActivity.Companion.productDetailsActivityRequestCode
import com.tomi.mobile.adapters.MenuGridAdapter
import com.tomi.mobile.databinding.ProductMainBinding
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.network.MenuProperty
import com.tomi.mobile.viewModels.CurrentOrderViewModel
import com.tomi.mobile.viewModels.MenuViewModel
import java.util.*

class ProductListFragment : androidx.fragment.app.Fragment(), MenuGridAdapter.OnItemClickListener {
    private lateinit var currentOrderViewModel: CurrentOrderViewModel
    private val menuViewModel: MenuViewModel by lazy {
        ViewModelProviders.of(this).get(MenuViewModel::class.java)
    }

    private lateinit var currentOrder: Order

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentOrderViewModel = activity?.run {
            ViewModelProviders.of(this).get(CurrentOrderViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = ProductMainBinding.inflate(inflater)
        binding.setLifecycleOwner(this)
        binding.viewModel = menuViewModel
        binding.productContainer.adapter = MenuGridAdapter(this)
        return binding.root
    }

    override fun onItemClick(menuProperty: MenuProperty) {
        var intent = Intent(activity, ProductDetailsActivity::class.java)
        getCurrentOrder()
        intent.putExtra("MenuProperty", menuProperty)
        intent.putExtra(ITEM_ID, UUID.randomUUID())
        startActivityForResult(intent, productDetailsActivityRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == productDetailsActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.let {
                val newItem = it.getSerializableExtra(ProductDetailsActivity.EXTRA_NEW_ITEM) as Item
                getCurrentOrder()
                if (currentOrder != null) {
                    currentOrder.itemList?.add(newItem)
                    currentOrderViewModel.edit(currentOrder!!)
                } else  {
                    Toast.makeText(context, "currentOrder not exist", Toast.LENGTH_LONG)
                }
            }
        }
    }

    private fun getCurrentOrder() {
        var tmp = currentOrderViewModel.order.value?.find { it.isCurrent }
        if (tmp == null) {
            val id = if (!currentOrderViewModel.order.value.isNullOrEmpty()) {
                currentOrderViewModel.order.value!!.last()?.id?.plus(1)
            } else {
                1
            }
            tmp = Order(id = id, priority = 1, telephone = "", isTakeaway = false, total = 0, paid = false
                , isCurrent = true)
            currentOrderViewModel.insert(tmp)
        }
        currentOrder = tmp
    }

    companion object {
        fun newInstance(): ProductListFragment = ProductListFragment()
        const val productListFragmentRequestCode = 55151
        const val ITEM_ID = "item_id"

    }
}
