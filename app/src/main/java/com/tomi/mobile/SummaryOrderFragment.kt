package com.tomi.mobile

import android.app.Activity
import android.content.Intent
import android.os.Build
import com.tomi.mobile.adapters.baseAdapter.SummaryItemAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.viewModels.CurrentOrderViewModel
import kotlinx.android.synthetic.main.content_summary_order.*

class SummaryOrderFragment : androidx.fragment.app.Fragment(), SummaryItemAdapter.CallbackInterface {

    private var mItemList = ArrayList<Item>()
    private lateinit var currentOrderViewModel: CurrentOrderViewModel
    private lateinit var adapter: SummaryItemAdapter
    private var currentOrder : Order? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentOrderViewModel = activity?.run {
            ViewModelProviders.of(this).get(CurrentOrderViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        currentOrder = currentOrderViewModel.order.value?.find { it.isCurrent }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_summary_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = context?.let { SummaryItemAdapter(it, this, mItemList) }!!
        summary_order_list.adapter = adapter
        currentOrderViewModel.order.observe(this, Observer {orderList ->
            currentOrder = orderList.find{ order -> order.isCurrent }
            if (currentOrder != null) {
                currentOrder!!.itemList?.let { itemList -> (summary_order_list.adapter as SummaryItemAdapter)
                    .setData(itemList)}
            } else {
                (summary_order_list.adapter as SummaryItemAdapter).setData(ArrayList())
            }
            (summary_order_list.adapter as SummaryItemAdapter).notifyDataSetChanged()
        })
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onHandleDelete(item: Item) {
        currentOrder?.itemList?.removeIf{
            it.id == item.id
        }
        currentOrder?.let { currentOrderViewModel.edit(it) }

    }

    override fun onHandleSelection(item: Item) {
        var intent = Intent(context, ProductDetailsActivity::class.java)
        intent.putExtra("item", item)
        startActivityForResult(intent, ProductListFragment.productListFragmentRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ProductListFragment.productListFragmentRequestCode && resultCode == Activity.RESULT_OK) {
            currentOrder = currentOrderViewModel.order.value?.find { it.isCurrent }
            data?.let {
                val passedItem = it.getSerializableExtra(ProductDetailsActivity.EXTRA_NEW_ITEM) as Item
                currentOrder?.itemList = currentOrder?.itemList?.map{ item ->
                    if (item.id == passedItem.id) {
                        passedItem
                    } else {
                        item
                    }
                } as ArrayList<Item>
                currentOrder?.let { it1 -> currentOrderViewModel.edit(it1) }
            }
        }
    }

    companion object {
        fun newInstance(): SummaryOrderFragment = SummaryOrderFragment()
    }
}
