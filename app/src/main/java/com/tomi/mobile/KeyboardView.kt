package com.tomi.mobile

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.keyboard.view.*


class KeyboardView : LinearLayout, View.OnClickListener{

    interface ButtonControl{
        fun onButtonClickControl(editText: EditText?)
    }

    private var mPasswordField: EditText? = null
    private lateinit var mButtonControl: ButtonControl

    constructor(context: Context) : super(context){
        init()
    }

    constructor(context: Context, popUpWindow: ConfirmPopUpWindow) : super(context){
        mButtonControl = popUpWindow
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr) {
        init()
    }

    init {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.keyboard, this)
        initViews()
    }

    private fun initViews() {
        mPasswordField = password_field
        t9_key_0.setOnClickListener(this)
        t9_key_1.setOnClickListener(this)
        t9_key_2.setOnClickListener(this)
        t9_key_3.setOnClickListener(this)
        t9_key_4.setOnClickListener(this)
        t9_key_5.setOnClickListener(this)
        t9_key_6.setOnClickListener(this)
        t9_key_7.setOnClickListener(this)
        t9_key_8.setOnClickListener(this)
        t9_key_9.setOnClickListener(this)
        t9_key_clear.setOnClickListener(this)
        t9_key_backspace.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.getTag() != null && "number_button" == v.getTag()) {
            mPasswordField!!.append((v as TextView).text)
            return mButtonControl.onButtonClickControl(mPasswordField)
        }
        when (v.getId()) {
            R.id.t9_key_clear -> {
                mPasswordField!!.setText(null)
            }
            R.id.t9_key_backspace -> {
                val editable = mPasswordField!!.text
                val charCount = editable.length
                if (charCount > 0) {
                    editable.delete(charCount - 1, charCount)
                }
            }
        }
        return mButtonControl.onButtonClickControl(mPasswordField)
    }
}