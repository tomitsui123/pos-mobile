package com.tomi.mobile

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.others.PrintService
import kotlinx.android.synthetic.main.content_popup_confirm.view.*

class ConfirmPopUpWindow() : KeyboardView.ButtonControl {

    interface Savable {
        fun saveData()
    }

    private var printCount = 0
    private var popupWindow : PopupWindow? = null
    lateinit var activity: Activity
    lateinit var printBtn : TextView
    lateinit var popUpView: View
    private lateinit var currentOrder : Order
    private lateinit var printService: PrintService

    @RequiresApi(Build.VERSION_CODES.M)
    constructor(context: Context, currentOrder: Order, printService: PrintService) : this() {
        this.activity = context as Activity
        this.currentOrder = currentOrder
        this.printService = printService

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun initPopUpWindow() {
//        printService.bind()
        var inflater = this.activity
            .getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        popUpView = inflater.inflate(R.layout.content_popup_confirm, null)
        popUpView.max_print_count.text = maxPrintCount.toString()
        popUpView.counted_print.text = printCount.toString()
        var keyboardView = KeyboardView(this.activity, popUpWindow = this)
        (popUpView.findViewById(R.id.popup_content) as LinearLayout).addView(keyboardView)
        popUpView.background.alpha = 150
        popupWindow = PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT, false)
        var closeBtn = popUpView.findViewById(R.id.close_popup) as ImageView
        closeBtn.setOnClickListener(onCloseWindow())
        printBtn = popUpView.findViewById(R.id.print_btn) as TextView
        printBtn.setOnClickListener(onPrintRecipe())

        var totalAmountTxt = popUpView.findViewById(R.id.total_amount) as TextView
        totalAmountTxt.text = currentOrder.total.toString()
    }

    fun onButtonControlPopUp() {
        popupWindow?.showAtLocation(this.activity.currentFocus, Gravity.CENTER, 0, 0)
    }

    private fun onPrintRecipe(): View.OnClickListener {
        return View.OnClickListener {
            printCount += 1
            popUpView.counted_print.text = printCount.toString()
            if (printCount < maxPrintCount) {
                printService.printReceipt(currentOrder)
            } else {
                printService.printReceipt(currentOrder)
                (activity as Savable).saveData()
                activity.finish()
            }
        }
    }

    private fun onCloseWindow(): View.OnClickListener {
        return View.OnClickListener {
            popupWindow?.dismiss()
        }
    }

    override fun onButtonClickControl(editText: EditText?) {
        if (editText != null) {
            var paidAmount = if (!editText?.text.toString().isBlank()) editText?.text.toString().toInt() else 0
            (popUpView.findViewById(R.id.paid_amount) as TextView).text = paidAmount.toString()
            (popUpView.findViewById(R.id.return_amount) as TextView).text =
                if(paidAmount != 0) (currentOrder.total - paidAmount).toString() else (0).toString()
        }
    }

    companion object {
        const val maxPrintCount = 3
    }
}