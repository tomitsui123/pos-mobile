package com.tomi.mobile.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tomi.mobile.network.MenuApi
import com.tomi.mobile.network.MenuProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MenuViewModel : ViewModel() {
    private val _property = MutableLiveData<List<MenuProperty>>()

    val property: LiveData<List<MenuProperty>>
        get() = _property

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main
    )

    init {
        getMenuProperties()
    }

    private fun getMenuProperties() {
        coroutineScope.launch {
            try {
                var getPropertiesDeferred = MenuApi.retrofitService.getPropertiesAsync()
                var listResult = getPropertiesDeferred.await()
                if (listResult.isNotEmpty()) {
                    _property.value = listResult
                }
            } catch (e: Exception) {
                print(e)
            }

        }
    }
}