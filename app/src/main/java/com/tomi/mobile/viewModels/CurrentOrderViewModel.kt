package com.tomi.mobile.viewModels

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.db.OrderRoomDatabase
import com.tomi.mobile.repositories.CurrentOrderRepository
import kotlinx.coroutines.launch

class CurrentOrderViewModel(application: Application) : AndroidViewModel(application)  {

    private val repository: CurrentOrderRepository
    val order: LiveData<List<Order>>

    init {
        val currentOrderDAO = OrderRoomDatabase.getDatabase(application, viewModelScope).currentOrderDAO()
        repository = CurrentOrderRepository(currentOrderDAO)
        order = repository.allOrder
    }

    fun insert(order: Order) = viewModelScope.launch {
        repository.insert(order)
    }

    fun edit(order: Order) = viewModelScope.launch {
        repository.edit(order)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun getData() = viewModelScope.launch {
        repository.fetchData()
    }

    fun syncOrder() = viewModelScope.launch {
        repository.syncOrder()
    }

}