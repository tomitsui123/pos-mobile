package com.tomi.mobile

import com.tomi.mobile.adapters.FragmentAdapters.OrderAdapter
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.viewModels.CurrentOrderViewModel
import kotlinx.android.synthetic.main.fragment_order.*


class OrderFragment : androidx.fragment.app.Fragment() {

    private lateinit var currentOrderViewModel: CurrentOrderViewModel
    private lateinit var currentOrder: Order

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentOrderViewModel = activity?.run {
            ViewModelProviders.of(this).get(CurrentOrderViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = OrderAdapter(childFragmentManager)
        view_pager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)
        complete_order_button.setOnClickListener{
            var intent = Intent(context, ConfirmActivity::class.java)
            if  (currentOrder != null) {
                intent.putExtra("id", currentOrder.id)
            }
            startActivity(intent)
        }
        currentOrderViewModel.order.observe(this, Observer {
             if (it.find{ order -> order.isCurrent} == null) {
                total_amount.text = "0"
            } else {
                 currentOrder = it.find{ order -> order.isCurrent}!!
                 complete_order_button.isEnabled = true
                 var total = 0
                 for (item in currentOrder.itemList!!) {
                     total += (item.amount * item.menuProperty.price)
                 }
                total_amount.text = total.toString()
            }
        })
    }

    fun newInstance(): OrderFragment {
        return OrderFragment()
    }
}
