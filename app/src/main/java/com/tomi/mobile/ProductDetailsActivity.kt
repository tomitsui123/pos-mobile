package com.tomi.mobile

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.tomi.mobile.ProductListFragment.Companion.ITEM_ID
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.network.MenuProperty
import kotlinx.android.synthetic.main.activity_product_details.*
import kotlinx.android.synthetic.main.content_product_details.*
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class ProductDetailsActivity : AppCompatActivity() {
    private var remarkList = ArrayList<String>()
    private var itemId = ""
    private var isTakeAway = false
    private var item: Item? = null
    private var editStatus = EXTRA_NEW_ITEM
    private var menuProperty : MenuProperty? = null


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        setSupportActionBar(toolbar)

        if (this.intent.hasExtra("item")) {
            item = this.intent.getSerializableExtra("item") as Item
            item?.let {
                menuProperty = it.menuProperty!!
                (num_of_items as EditText).setText(it.amount.toString())
                remarkList = it.remarkList!!
                isTakeAway = it.isTakeAway
            }
            editStatus = EDIT_EXISTING_ITEM
        } else {
            menuProperty = this.intent.getSerializableExtra("MenuProperty") as MenuProperty
        }
        if (this.intent.hasExtra(ITEM_ID)) {
            itemId = (this.intent.getSerializableExtra(ITEM_ID) as UUID).toString()
        }
        if (menuProperty == null) {
            return
        }
        var actionbar = supportActionBar
        menuProperty?.let {
            actionbar!!.title = it.displayName
            actionbar.setDisplayHomeAsUpEnabled(true)
            item_code.text = it.itemCode
            item_name.text = it.displayName
            price.text = it.price.toString()
            for (option in it.options) {
                createAdditionalRequest(option.title,"${option.title}：", option.type, option.option as ArrayList<String>)
            }
        }
        is_take_away.isChecked = isTakeAway
        is_take_away.setOnCheckedChangeListener { _, isChecked ->
            isTakeAway = isChecked
        }
        confirm_button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(num_of_items.text)) {
                Toast.makeText(
                    this,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show()
            } else {
                if (item == null) {
                    item = Item(itemId, menuProperty!!,
                        num_of_items.text.toString().toInt(), remarkList, isTakeAway)
                }
                item!!.amount = num_of_items.text.toString().toInt()
                replyIntent.putExtra(editStatus, item as Serializable)
                setResult(Activity.RESULT_OK, replyIntent)
                finish()
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(Build.VERSION_CODES.M)
    private fun createAdditionalRequest(id: String, title: String, optionType: String, optionList: ArrayList<String>) {
        var container = createBaseLinearLayout()
        var titleTv = TextView(this)
        titleTv.text = title
        titleTv.setTextAppearance(R.style.fontSizeForItemDetails)
        container.addView((titleTv))
        if (optionType == "checkbox") {
            var optionContainer = LinearLayout(this)
            optionContainer.orientation = LinearLayout.HORIZONTAL
            for (option in optionList) {
                var optionCb = CheckBox(this)
                optionCb.text = option
                optionCb.setTextAppearance( R.style.fontSizeForItemDetails)
                optionCb.isChecked = option in remarkList
                optionContainer.addView(optionCb)
                optionCb.setOnCheckedChangeListener { buttonView, isChecked ->
                    println(isChecked)
                    val text = "$title -> ${buttonView.text.toString()}"
                    if (isChecked) {
                        remarkList.add(text)
                    } else  {
                        remarkList.removeIf {
                            it == buttonView.text.toString()
                        }
                    }
                }
            }
            container.addView(optionContainer)
        } else {
            var optionGroup = RadioGroup(this)
            optionGroup.orientation = LinearLayout.HORIZONTAL
            for (option in optionList) {
                var optionRb = RadioButton(this)
                optionRb.text = option
                optionRb.setTextAppearance(R.style.fontSizeForItemDetails)
                optionRb.setOnClickListener {
                    val text = "$title -> ${(it as RadioButton).text}"
                    remarkList.removeIf { str ->str.contains(title) }
                    remarkList.add(text)
                }
                optionGroup.addView(optionRb)
                if (remarkList.any { it.split("->")[1].trim() == option }) {
                    optionGroup.check(optionRb.id)
                }
            }
            container.addView(optionGroup)
        }
        additional_request.addView(container)

    }

    private fun createBaseLinearLayout(): LinearLayout {
        var base = LinearLayout(this)
        base.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        base.orientation = LinearLayout.VERTICAL
        return base
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onOptionsItemSelected(mItem: MenuItem): Boolean {
        when(mItem.itemId) {
            R.id.confirm -> {
                val replyIntent = Intent()
                if (TextUtils.isEmpty(num_of_items.text)) {
                    Toast.makeText(
                        this,
                        R.string.empty_not_saved,
                        Toast.LENGTH_LONG).show()
                } else {
                    if (item == null) {
                        item = Item(itemId, menuProperty!!, num_of_items.text.toString().toInt(), remarkList, isTakeAway)
                    }
                    item!!.amount = num_of_items.text.toString().toInt()
                    replyIntent.putExtra(editStatus, item as Serializable)
                    setResult(Activity.RESULT_OK, replyIntent)
                    finish()
                }
            }
        }
        return super.onOptionsItemSelected(mItem)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_order, menu)
        return super.onCreateOptionsMenu(menu)
    }

    companion object {
        const val EXTRA_NEW_ITEM = "NEW_ITEM"
        const val EDIT_EXISTING_ITEM = "EDIT_ITEM"
        const  val productDetailsActivityRequestCode = 23235
    }
}
