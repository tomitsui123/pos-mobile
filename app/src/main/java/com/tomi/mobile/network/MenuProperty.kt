package com.tomi.mobile.network

import com.squareup.moshi.Json
import java.io.Serializable

class MenuProperty(
    @Json(name = "_id")
    val id: String,
    val category: String,
    val options: List<optionProperty>,
    val itemCode: String,
    val displayName: String,
    val price: Int,
    val imgSrcUrl: String
): Serializable

class optionProperty(
    @Json(name = "title")
    val title: String,
    @Json(name = "type")
    val type: String,
    val option: List<String>
): Serializable