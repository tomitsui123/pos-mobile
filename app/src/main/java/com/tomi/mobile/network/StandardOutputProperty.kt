package com.tomi.mobile.network

class StandardOutputProperty (
    val message: String,
    val id: String
)