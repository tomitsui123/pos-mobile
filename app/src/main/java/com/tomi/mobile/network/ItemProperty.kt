package com.tomi.mobile.network

import com.squareup.moshi.Json
import com.tomi.mobile.db.entities.Item

class ItemProperty(
    @Json(name = "item")
    val item: String,
    @Json(name = "menuProperty")
    val menuProperty: MenuProperty,
    @Json(name = "price")
    val price: Int,
    @Json(name = "amount")
    val amount: Int,
    @Json(name = "remarkList")
    val remarkList: List<String>?,
    val itemNumber: String? = ""
) {
    fun toItem():Item {
        return Item(
            id = itemNumber,
            amount = amount,
            menuProperty = menuProperty,
            isTakeAway = false,
            remarkList = ArrayList(remarkList)
        )
    }
}