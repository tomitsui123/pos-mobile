package com.tomi.mobile.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tomi.mobile.others.MyApp
import com.tomi.mobile.others.hasNetwork
import kotlinx.coroutines.Deferred
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

//private const val BASE_URL = "http://10.0.2.2/api/"
//private const val BASE_URL = "http://192.168.1.75/api/"
private const val BASE_URL = "http://172.20.10.3/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val cacheSize = (5 * 1024 * 1024).toLong()
private val context = MyApp.context
private val myCache: Cache = Cache(context?.cacheDir, cacheSize)

private val okHttpClient: OkHttpClient = OkHttpClient.Builder()
    .cache(myCache)
    .addInterceptor { chain ->
        var request = chain.request()
        request = if (context?.let { hasNetwork(it) }!!)
            request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
        else
            request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
        chain.proceed(request)
    }
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .client(okHttpClient)
    .baseUrl(BASE_URL)
    .build()

interface MenuApiService {
    @GET("recipe")
    fun getPropertiesAsync():
            Deferred<List<MenuProperty>>
}

object MenuApi {
    val retrofitService : MenuApiService by lazy {
        retrofit.create(MenuApiService::class.java)
    }
}