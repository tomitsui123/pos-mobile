package com.tomi.mobile.network

import com.squareup.moshi.Json
import com.tomi.mobile.db.entities.Order
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class OrderProperty(
    @Json(name = "_id")
    val id: String? = "",
    @Json(name = "orderNumber")
    val orderNumber: Int?,
    @Json(name = "isTakeAway")
    val isTakeAway: Boolean = false,
    @Json(name = "telephone")
    val telephone: String?,
    @Json(name = "priority")
    val priority: Int,
    @Json(name = "paid")
    val paid: Boolean,
    @Json(name = "remarks")
    val remarks: List<String>,
    @Json(name = "deleted")
    val deleted: Boolean,
    @Json(name = "createdAt")
    val createdAt: Date,
    @Json(name = "updatedAt")
    val updatedAt: Date = Date(),
    @Json(name = "total")
    val total: Int = 0,
    @Json(name = "itemList")
    val itemList: List<ItemProperty>
): Serializable {
    fun toOrder(): Order {
        return Order(
            orderNumber, priority = priority, telephone = telephone
            , isTakeaway = isTakeAway, total = total, paid = paid, orderId = id,
            isSync = true, itemList = ArrayList(itemList.map { it.toItem() }))
    }
}