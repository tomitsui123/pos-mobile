package com.tomi.mobile.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tomi.mobile.db.DAO.CurrentOrderDAO
import com.tomi.mobile.db.converter.DateConverter
import com.tomi.mobile.db.converter.ItemConverter
import com.tomi.mobile.db.converter.StringListConverter
import com.tomi.mobile.db.entities.Order
import kotlinx.coroutines.CoroutineScope

@Database(entities = [Order::class], version = 22)
@TypeConverters(StringListConverter::class, ItemConverter::class, DateConverter::class)
abstract class OrderRoomDatabase : RoomDatabase() {
    abstract fun currentOrderDAO(): CurrentOrderDAO

    private class CurrentOrderDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

//        override fun onOpen(db: SupportSQLiteDatabase) {
//            super.onOpen(db)
//            INSTANCE?.let { database ->
//                scope.launch {
//                    populateDatabase(database.currentOrderDAO())
//                }
//            }
//        }

        suspend fun populateDatabase(currentOrderDAO: CurrentOrderDAO) {
            currentOrderDAO.deleteAll()
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: OrderRoomDatabase? = null
        fun getDatabase(context: Context, scope: CoroutineScope): OrderRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    OrderRoomDatabase::class.java,
                    "current_order_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(CurrentOrderDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}