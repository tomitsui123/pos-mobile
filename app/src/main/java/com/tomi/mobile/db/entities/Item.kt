package com.tomi.mobile.db.entities

import com.squareup.moshi.Json
import com.tomi.mobile.network.ItemProperty
import com.tomi.mobile.network.MenuProperty
import java.io.Serializable


data class Item (
    val id: String?,
    val menuProperty: MenuProperty,
    var amount: Int,
    @Json(name = "remark")
    val remarkList: ArrayList<String>?,
    val isTakeAway: Boolean
) : Serializable {
    fun toItemProperty(): ItemProperty {
        return  ItemProperty(
            item = menuProperty.displayName,
            price = menuProperty.price,
            amount = amount,
            remarkList = remarkList,
            menuProperty = menuProperty,
            itemNumber = id)
    }
}
