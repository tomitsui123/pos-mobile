package com.tomi.mobile.db.converter

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson
import com.tomi.mobile.db.entities.Order


class OrderArrayListConverter {
    private val gson = Gson()
    private val orderArrayListType = object : TypeToken<ArrayList<Order>>() {
    }.type

    @TypeConverter
    fun orderArrayListFromJsonArray(json: String): ArrayList<Order> {
        return gson.fromJson(json, orderArrayListType)
    }

    @TypeConverter
    fun orderArrayListToJsonArray(stringList: ArrayList<Order>): String {
        return gson.toJson(stringList)
    }
}