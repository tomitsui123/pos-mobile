package com.tomi.mobile.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tomi.mobile.network.OrderProperty
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


@Entity(tableName = "order_table")
data class Order(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = 0,
    @ColumnInfo(name = "priority")
    var priority: Int,
    @ColumnInfo(name = "telephone")
    var telephone: String?,
    @ColumnInfo(name = "isTakeAway")
    var isTakeaway: Boolean,
    @ColumnInfo(name = "total")
    var total: Int,
    @ColumnInfo(name = "paid")
    var paid: Boolean,
    @ColumnInfo(name = "created_date")
    var createdDate: Date = Date(),
    var orderRemarkList: ArrayList<String> = ArrayList(),
    var itemList: ArrayList<Item> = ArrayList(),
    var isCurrent: Boolean = false,
    var isSync: Boolean = false,
    var orderId: String? = "",
    var isEdited: Boolean = false
) : Serializable {
    fun toOrderProperty(): OrderProperty {
        return OrderProperty(orderId, id, isTakeaway, telephone, priority, paid, orderRemarkList.toList(), false, createdDate, total = total,
            itemList = itemList.map { it.toItemProperty() })
    }
}