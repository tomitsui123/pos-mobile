package com.tomi.mobile.db.converter

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson


public class StringListConverter {
    private val gson = Gson()
    private val orderRemarkType = object : TypeToken<ArrayList<String>>() {

    }.type

    @TypeConverter
    fun orderRemarksFromJsonArray(json: String): ArrayList<String> {
        return gson.fromJson(json, orderRemarkType)
    }

    @TypeConverter
    fun orderRemarksToJsonArray(stringList: ArrayList<String>): String {
        return gson.toJson(stringList)
    }
}