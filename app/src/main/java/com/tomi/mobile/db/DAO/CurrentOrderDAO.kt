package com.tomi.mobile.db.DAO

import androidx.lifecycle.LiveData
import androidx.room.*
import com.tomi.mobile.db.entities.Order

@Dao
interface CurrentOrderDAO {

    @Query("SELECT * from order_table")
    fun getCurrentOrder(): LiveData<List<Order>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(order: Order): Long

    @Query("DELETE FROM order_table")
    suspend fun deleteAll()

    @Query("DELETE FROM order_table WHERE isSync = 1")
    suspend fun deleteSyncedOrder()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun edit(order: Order)

    @Query("UPDATE order_table SET isSync = 1 where id = :orderId")
    suspend fun setSyncIsTrue(orderId: Int)

    @Query("UPDATE order_table SET isEdited = 0 where id = :orderId")
    suspend fun setEditedIsFalse(orderId: Int)

    @Query("SELECT id FROM order_table ORDER BY id DESC LIMIT 1")
    suspend fun getLastOrderId(): Long

}