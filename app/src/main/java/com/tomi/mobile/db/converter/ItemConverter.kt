package com.tomi.mobile.db.converter

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson
import com.tomi.mobile.db.entities.Item

class ItemConverter {
    private val gson = Gson()
    private val itemType = object : TypeToken<ArrayList<Item>>(){}.type

    @TypeConverter
    fun itemFromJsonArray(json: String): ArrayList<Item> {
        return gson.fromJson(json, itemType)
    }

    @TypeConverter
    fun itemToJsonArray(items: ArrayList<Item>): String {
        return gson.toJson(items)
    }
}