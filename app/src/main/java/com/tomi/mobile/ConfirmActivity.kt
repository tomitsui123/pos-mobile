package com.tomi.mobile


import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.adapters.baseAdapter.SummaryItemAdapter
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.others.PrintService
import com.tomi.mobile.viewModels.CurrentOrderViewModel
import kotlinx.android.synthetic.main.activity_confirm.*
import kotlinx.android.synthetic.main.content_confirm.*
import kotlinx.android.synthetic.main.content_summary_order.*
import java.util.*
import kotlin.collections.ArrayList


class ConfirmActivity : AppCompatActivity(), SummaryItemAdapter.CallbackInterface, ConfirmPopUpWindow.Savable {

    private lateinit var currentOrderViewModel: CurrentOrderViewModel
    private lateinit var confirmPopUpWindow : ConfirmPopUpWindow
    private lateinit var currentOrder : Order
    private var totalAmount = 0
    private var id = 0
    private var printService: PrintService = PrintService(this)
    private var mMenu: Menu? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm)
        setSupportActionBar(toolbar)
        var actionbar = supportActionBar
        actionbar!!.title = "結帳"
        actionbar.setDisplayHomeAsUpEnabled(true)
        printService.bind()

        currentOrderViewModel = this.run {
            ViewModelProviders.of(this).get(CurrentOrderViewModel::class.java)
        }
        var orderedItemList = ArrayList<Item>()

        summary_order_list.adapter = SummaryItemAdapter(this, null, orderedItemList)
        if (this.intent.hasExtra("id")) {
            id = this.intent.getSerializableExtra("id") as Int
        }
        currentOrderViewModel.order.observe(this, Observer {orderList ->
            orderList.find { it.id === id }?.let { it ->
                currentOrder = it
                mMenu?.let {
                    val editBtn = it.getItem(1)
                    editBtn.isVisible = !currentOrder.isCurrent
                    val printBtn = it.getItem(2)
                    printBtn.isVisible = !currentOrder.isCurrent
                }
                totalAmount = 0
                for (item in currentOrder.itemList!!) {
                    totalAmount += (item.amount * item.menuProperty.price)
                }
                currentOrder.total = totalAmount
                confirmPopUpWindow = ConfirmPopUpWindow(this, currentOrder, printService)
                confirmPopUpWindow.initPopUpWindow()
                telephone.setText(currentOrder.telephone)
                paid_switch.isChecked = currentOrder.paid
                total_price.text = totalAmount.toString()
                val summaryItemAdapter = (summary_order_list.adapter as SummaryItemAdapter?)
                summaryItemAdapter?.let{
                    currentOrder.itemList?.let { it1 -> it.setData(it1) }
                    it.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onDestroy() {
        printService.unbind()
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.confirm_order, menu)
        mMenu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.confirm_order -> {
                currentOrder.let {
                    if (it.isCurrent) {
                        it.createdDate = Date()
                    } else {
                        if (!it.isEdited) {
                            it.isEdited = !it.telephone.equals(telephone.text.toString()) || it.paid != paid_switch.isChecked
                        }
                    }
                    it.telephone = telephone.text.toString()
                    it.paid = paid_switch.isChecked
                    currentOrderViewModel.edit(it)
                }
                if (currentOrder.isCurrent) {
//                    create order
                    confirmPopUpWindow.onButtonControlPopUp()
                } else {
//                    edit order
                    finish()
                }
            }
            R.id.print_receipt -> {
                printService.printReceipt(currentOrder)
            }
            R.id.edit_order -> {
                var intent = Intent(this, EditOrderActivity::class.java)
                intent.putExtra(CURRENT_ORDER, currentOrder)
                startActivityForResult(intent, EditOrderActivity.editOrderActivityRequestCode)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    EditOrderActivity.editOrderActivityRequestCode -> {
//                        add new item
                        data?.let { it ->
                            val newItem = it.getSerializableExtra(ProductDetailsActivity.EXTRA_NEW_ITEM) as Item
                            currentOrder?.let {order ->
                                order.itemList?.add(newItem)
                            }
                        }
                    }
                    ProductDetailsActivity.productDetailsActivityRequestCode -> {
//                        edit existing item
                        data?.let {
                            val newItem = it.getSerializableExtra(ProductDetailsActivity.EDIT_EXISTING_ITEM) as Item
                            currentOrder?.let {order ->
                                order.itemList = order.itemList
                                    .map { item ->
                                        if (item.id.equals(newItem.id)) {
                                            order.isEdited = item != newItem
                                            newItem
                                        } else {
                                            item
                                        }
                                    } as ArrayList<Item>
                            }
                        }

                    }
                }
                currentOrderViewModel.edit(currentOrder)
            }
        }
    }


    override fun saveData() {
        currentOrder?.isCurrent = false
        currentOrder?.let { currentOrderViewModel.edit(it) }
        currentOrderViewModel.syncOrder()
    }

    override fun onHandleSelection(item: Item) {
        println("selected")
        println(item.menuProperty.displayName)
        var intent = Intent(this, ProductDetailsActivity::class.java)
        intent.putExtra("item", item)
        startActivityForResult(intent, ProductDetailsActivity.productDetailsActivityRequestCode)
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onHandleDelete(item: Item) {
        println("deleted")
        println(item.menuProperty.displayName)
        val builder =  AlertDialog.Builder(this)
        builder.setTitle("確定刪除項目 ${item.menuProperty.displayName}?")
        builder.setPositiveButton("是") { dialog, which ->
            currentOrder.itemList.removeIf{ it.id === item.id }
            currentOrderViewModel.edit(currentOrder)
        }
        builder.setNegativeButton("否") { dialog, which ->
            println("Cancel delete")
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    companion object {
        const val CURRENT_ORDER = "current_order"
        const val confirmActivityRequestCode = 41325
    }
}
