package com.tomi.mobile.repositories

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.tomi.mobile.db.DAO.CurrentOrderDAO
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.network.OrderApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody


class CurrentOrderRepository(private val currentOrderDao: CurrentOrderDAO) {

    val allOrder: LiveData<List<Order>> = currentOrderDao.getCurrentOrder()

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main
    )

    suspend fun insert(order: Order): Long {
        return currentOrderDao.insert(order)
    }

    suspend fun edit(order: Order) {
        order.itemList.sortBy { it.menuProperty.category }
        currentOrderDao.edit(order)
    }

    private suspend fun deleteSyncedOrder() {
        currentOrderDao.deleteSyncedOrder()
    }

    private suspend fun setSyncIsTrue(order: Order) {
        order.id?.let { currentOrderDao.setSyncIsTrue(it) }
    }

    private suspend fun setEditedIsFalse(order: Order) {
        order.id?.let { currentOrderDao.setEditedIsFalse(it) }
    }



    @RequiresApi(Build.VERSION_CODES.N)
    suspend fun fetchData() {
        syncOrder()
        getRecipesFromWeb()
    }

    suspend fun syncOrder() {
        coroutineScope.launch {
            try {
                val toSyncOrder = allOrder.value?.filter { !it.isSync || it.isEdited }
                toSyncOrder?.let {
                    for (order in toSyncOrder) {
                        val json = Gson().toJson(order.toOrderProperty())
                        val orderRB = RequestBody.create(MediaType.parse("application/json"), json.toString())
                        var sendRetrofitPost = if (!order.isEdited) {
                            OrderApi.retrofitService.createOrderAsync(orderRB)
                        } else {
                            order.orderId?.let { orderId ->
                                OrderApi.retrofitService.editOrderAsync(orderId, orderRB)
                            }
                        }
                        var response = sendRetrofitPost?.await()
                        response?.let {
                            setSyncIsTrue(order)
                            setEditedIsFalse(order)
                            println(response.message)
                        }
                    }
                }
            } catch (e: Exception) {
                println(e)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private suspend fun getRecipesFromWeb() {
        coroutineScope.launch {
            try {
                var getPropertiesDeferred = OrderApi.retrofitService.getPropertiesAsync()
                var listResult = getPropertiesDeferred.await()
                deleteSyncedOrder()
                if (listResult.isNotEmpty()) {
                    for (orderProperty in listResult) {
                        insert(orderProperty.toOrder())
                    }
                }
            } catch (e: Exception) {
                print(e)
            }

        }
    }
}