package com.tomi.mobile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.ProductDetailsActivity.Companion.productDetailsActivityRequestCode
import com.tomi.mobile.adapters.MenuGridAdapter
import com.tomi.mobile.databinding.ActivityEditOrderBinding
import com.tomi.mobile.databinding.ProductMainBinding
import com.tomi.mobile.db.entities.Item
import com.tomi.mobile.db.entities.Order
import com.tomi.mobile.network.MenuProperty
import com.tomi.mobile.viewModels.MenuViewModel

import kotlinx.android.synthetic.main.activity_edit_order.*
import java.util.*

class EditOrderActivity : AppCompatActivity(), MenuGridAdapter.OnItemClickListener {

    private lateinit var currentOrder: Order

    private val menuViewModel: MenuViewModel by lazy {
        ViewModelProviders.of(this).get(MenuViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var binding = setContentView(this, R.layout.activity_edit_order) as ActivityEditOrderBinding
        setSupportActionBar(toolbar)
        var actionbar = supportActionBar
        actionbar!!.title = "加單"
        actionbar.setDisplayHomeAsUpEnabled(true)
        binding.setLifecycleOwner(this)
        binding.viewModel = menuViewModel
        binding.productMain.productContainer.adapter = MenuGridAdapter(this)
        if (this.intent.hasExtra(ConfirmActivity.CURRENT_ORDER)) {
            currentOrder = this.intent.getSerializableExtra(ConfirmActivity.CURRENT_ORDER) as Order
        }
    }

    override fun onItemClick(menuProperty: MenuProperty) {
        var intent = Intent(this, ProductDetailsActivity::class.java)
        intent.putExtra("MenuProperty", menuProperty)
        intent.putExtra(ProductListFragment.ITEM_ID, UUID.randomUUID())
        startActivityForResult(intent, productDetailsActivityRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == productDetailsActivityRequestCode
            && resultCode == Activity.RESULT_OK) {
            data?.let {
                val newItem = it.getSerializableExtra(ProductDetailsActivity.EXTRA_NEW_ITEM) as Item
                val replyIntent = Intent()
                replyIntent.putExtra(ProductDetailsActivity.EXTRA_NEW_ITEM, newItem)
                setResult(Activity.RESULT_OK, replyIntent)
                finish()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        const val editOrderActivityRequestCode = 1
        const val EXTRA_NEW_ITEM = "NEW_ITEM"
    }
}
