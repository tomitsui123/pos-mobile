package com.tomi.mobile

import com.tomi.mobile.adapters.baseAdapter.RecordAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.tomi.mobile.viewModels.CurrentOrderViewModel
import kotlinx.android.synthetic.main.content_record.*
import kotlin.collections.ArrayList
import androidx.lifecycle.Observer
import com.tomi.mobile.db.entities.Order

class RecordFragment : androidx.fragment.app.Fragment() {

    private lateinit var currentOrderViewModel: CurrentOrderViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        currentOrderViewModel = activity?.run {
            ViewModelProviders.of(this).get(CurrentOrderViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        currentOrderViewModel.getData()
        return inflater.inflate(R.layout.fragment_record, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var orderList = ArrayList<Order>()
        var adapter = activity?.let { RecordAdapter(it, orderList) }
        record_list.adapter = adapter
        currentOrderViewModel.order.observe(this, Observer {
            (record_list.adapter as RecordAdapter).let { adapter ->
                adapter.setData(ArrayList(it.filter { !it.isCurrent }))
                adapter.notifyDataSetChanged()
            }
        })
    }

    fun newInstance(): RecordFragment {
        return RecordFragment()
    }
}
